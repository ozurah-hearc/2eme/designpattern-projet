/*----------------------------------
  Design Patterns
  Projet
  Equipe 04 :   M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

1. Structure des codes sources
    1.1 Dossier "Command"
        - ICommand : interface commune aux commandes ;
        - SearchCommand : implémentation de l'interface pour la recherche.

    1.2 Dossier "Composite"
        - IFolder : interface "component" & "ConcreteCollection", interface pour les dossiers, permet l'ajout ou la suppression d'éléments, étend l'interface IIterable ;
        - File : classe de base pour les fichiers et dossiers ;
        - FolderList : classe "composite", dossiers pouvant contenir d'autres component, hérite de File et implémente IFolder ;
        - Document : classe "leaf", informations des fichiers, hérite de File.

    1.3 Dossier "Iterator"
        - IIterable : interface "IterableCollection", interface pour rendre une collection itérable par un "iterator" ;
        - IIterator : interface "Iterator", interface pour parcourir une collection ;
        - FolderListDepthIterator : classe "ConcreteIterator", logique d'itération pour des objets de type "FolderList", implémente IIterator ;

    1.4 Autres fichiers
        - MainWindow.xaml : description XML de la vue ;
        - MainWindow.xaml.cs : logique métier de la vue ;
        - Les fichiers non spécifiés sont propres aux projets Visual Studio pour le C#.



2. Améliorations possibles

    Parmi les améliorations qui pourraient être apportées à l'utilisation des designs patterns implémentés, les points intéressants seraient.

    2.1. Séparation du pattern Command
        Actuellement, celui-ci possède le même "Invoker" que "Receiver".  C'est pour cette raison qu'il y a moins de classes que le diagramme UML de base.
    
    2.2. Recherche
        La recherche ne permet que de faire des recherches sur des noms précis. Il n'est pas possible d'effectuer une recherche sur le chemin complet.
    
    2.3. En addition à cette dernière, la recherche s'arrête dès la première occurrence valide trouvée. La recherche ne retourne que le premier résultat.
        Concernant l'implémentation du programme, nous nous sommes limités aux fonctionnalités de bases présentes dans la plupart des langages. De ce fait, nous n'exploitons à aucun moment les performances offertes par C#.



3. Design patterns utilisés
    3.1 Composite
        Le composite est un design pattern de type structurel.
        Son but est de permettre l'agencement des objets dans une arborescence dans le but de traiter celle-ci comme un objet unique.
        Il est principalement composé de 3 classes : 
            - "Component" : une interface ayant les opérations communes pour les objets de l'arborescence ;
            - "Composite" : pouvant contenir des sous-éléments (composites, feuilles) ;
            - "Leaf"      : une terminaison, à l'instar d'une feuille.

        Pour mieux comprendre le "composite", prenons un exemple :
            On commande sur un site de vente en ligne plusieurs articles, disons par exemple : un nouveau téléphone, un casque Bluetooth,
            une paire de chaussures et une armoire en pièces détachées.

            A la livraison, nous recevrons un gros carton dans lequel nous pourrons trouver une boîte avec notre téléphone, une seconde
            boîte pour le casque, une troisième boîte contenant la paire de chaussures, et finalement un autre paquet contenant lui-même de plus petits paquets
            avec les pièces détachées de notre meuble.

            Maintenant, nous cherchons à savoir combien de temps cela nous prendra de déballer, configurer ou construire chacun des articles.

            C'est sur ce principe que s'appuie le pattern composite.
            
            Dans ce pattern, on va demander individuellement à chaque article "Combien de temps prends-tu ?" afin d'estimer le temps total pour déballer l'entièreté du colis.
            
            Dans notre exemple, les cartons correspondent aux objets "composites". Les articles correspondent aux objets "leaf".
            La fonction "Combien de temps prends-tu" est abstraite dans la classe "component". Cette dernière va être implémentée dans les classes "composite" et "leaf".

        (a) Faiblesse
            - Il peut être difficile de définir une interface commune à tous nos composites lorsqu'ils deviennent complexes.

        (b) Forces
            - Utilise les avantages du polymorphisme et de la récursivité ;
            - permet de traiter toute une arborescence comme étant un seul objet ;
            - la manipulation de chaque élément (feuilles et composites) de manière identique ;
            - possibilité d'ajouter ou supprimer simplement des éléments dans l'arborescence.


    3.2 Itérateur
        L'itérateur est un design pattern de type comportemental.
        Son but est de permettre le parcours d'une collection sans révéler sa structure interne.
        Il est composé de quatre éléments :
            - Iterator              : interface de déclaration des deux opérations nécessaires ;
            - Iterable              : interface commune aux collections itérables déclarant l'opération retournant un itérateur ;
            - Concrete Iterator     : itérateur concret, implémente les méthodes d'Iterator ;
            - Concrete Collection   : itérable concret, implémente les méthodes d'Iterable.

        Pour mieux comprendre l'itérateur, imaginons que nous souhaitions parcourir un arbre. Nous pourrions le parcourir en premier en profondeur, mais nous pourrions aussi décider de le parcourir en premier en largeur. Le code de l'arbre ne devrait pas se préoccuper de l'algorithme que nous allons utiliser pour le parcourir et nous implémenterons des classes itérateur pour se charger de cette fonction.

        (a) Faiblesse
            - Inutile si les collections sont simples. (tableau, liste, etc.)

        (b) Force(s)
            - Responsabilité unique : la collection n'est pas responsable de l'algorithme de parcours, mais uniquement de stocker les données de manière efficace ;
            - ouvert/fermé : on peut ajouter de nouveaux types de collections ou d'itérateurs sans toucher au code existant ;
            - état indépendant : l'itérateur conservant son état, il est possible d'interrompre l'itération pour la reprendre plus tard.


    3.3 Commande
        La Commande est un design pattern de type comportemental.
        Son but est de transformer une action à effectuer en objet autonome.
        Il est composé de quatre éléments :
            - Invoker           : l'invoker déclenche les commandes pour transmettre des demandes au récepteur ;
            - Receiver          : le receiver porte la logique.
            - Command           : l'interface command déclare une méthode d'exécution ;
            - Concrete Command  : implémente l'interface command et transmet les demandes au receiver ;

        (a) Faiblesse
            - Création d'une couche d'abstraction supplémentaire entre les demandeurs et les receveurs.
        (b) Forces
            - Responsabilité unique : découplage des classes qui demandent de celles qui exécutent les demandes ;
            -  Ouvert/fermé : ajouter une nouvelle commande ne demande pas de modifier le code existant ;
            -  Réutilisation : une commande instanciée peut être réutilisée pour être exécutée à de multiples reprises.



4. Conclusion sur ce labo

Concernant le design pattern Compsite, il est adapté pour les collections sous forme d'arbres. Cependant, parcourir un arbre en profondeur ou en largeur peut s'avérer complexe.

L'itérateur peut se coupler au composite, ce qui permet d'uniformiser les méthodes de parcours que l'ont pourrait implémenter. Par exemple, si nous souhaitions implémenter un parcours en largeur en plus du parcours en profondeur actuel, il nous suffirait d'ajouter un itérateur.
Ainsi, les algorithmes d'itération ne sont plus développés dans l'arbre, mais dans les itérateurs.

Quant au pattern command, il permet de réexecuter des opérations passées. Pour notre projet, il permet de réafficher les tailles des dossiers déjà obtenus en appliquant à nouveau la recherche enregistrée, sans que l'utilisateur doive, une fois de plus, saisir le nom du fichier. 


5. Sources

https://www.geeksforgeeks.org/software-design-patterns/
https://refactoring.guru/fr
Livre "La tête la première dans les designs patterns" http://bliaudet.free.fr/IMG/pdf/DPTLP.pdf 
﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

namespace ProjetDP.Command
{
    /// <summary>
    /// Classe implémentant l'interface ICommand, permet de réexecuter une recherche déjà effectuée
    /// </summary>
    public class SearchCommand : ICommand
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                               CHAMPS                              *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        private readonly string query;
        private readonly MainWindow mainWindow;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                            CONSTRUCTEURS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public SearchCommand(string query, MainWindow mainWindow)
        {
            this.query = query;
            this.mainWindow = mainWindow;
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                           METHODES PUBLIC                         *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public void Execute()
        {
            mainWindow.Search(query);
        }
    }
}

﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

namespace ProjetDP.Iterator
{
    /// <summary>
    /// Classe permettant de parcourire des collections (pouvant être sur plusieurs niveaux, tel qu'un arbre)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIterator<T>
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                           METHODES PUBLIC                         *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Element actuel de l'iteration dans la collection
        /// </summary>
        public T Current { get; }

        /// <summary>
        /// Profondeur de l'iteration
        /// </summary>
        public int Depth { get; }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
        |*                             PROPRIETES                            *|
        \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Déplacement à l'élément suivant dans la collection
        /// </summary>
        /// <returns></returns>
        public bool MoveNext();

        /// <summary>
        /// Remise à 0 de l'itérateur
        /// </summary>
        public void Reset();
    }
}

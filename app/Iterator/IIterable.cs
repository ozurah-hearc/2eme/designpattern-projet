﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

namespace ProjetDP.Iterator
{
    /// <summary>
    /// Classe permettant à des collections d'être itérable par <see cref="IIterator{T}"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIterable<T>
    {
        /// <summary>
        /// Obtient un iterateur pour la collection
        /// </summary>
        /// <returns></returns>
        public IIterator<T> GetIterator();
    }
}

﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

using ProjetDP.Composite;
using System.Collections.Generic;

namespace ProjetDP.Iterator
{
    /// <summary>
    /// Permet de faire une itération en profondeur sur un dossier.
    /// Le 1er élément de l'itération sera la racine en elle-même
    /// </summary>
    class FolderListDepthIterator : IIterator<File>
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
        |*                               CHAMPS                              *|
        \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        FolderList source; // Source de base de l'iterator (le tout 1er element)

        readonly Stack<FolderList> memory = new Stack<FolderList>(); // Pour le parcours autravers de chaque sous dossiers
        readonly Stack<int> index = new Stack<int>(); // Index actuel dans le sous-dossier en cours

        bool rootReaded = false; // L'iterateur commence par afficher l'élément racine

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
        |*                             PROPRIETES                            *|
        \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public File Current
        {
            get
            {
                if (!rootReaded) // On force la lecture de l'élément source
                    return source;

                if (memory.Peek().Files.Count == 0)
                    return null;

                return memory.Peek().Files[index.Peek()];
            }
        }

        public int Depth
        {
            get
            {
                if (!rootReaded) // On force la lecture de l'élément source (il a une profondeur de 0)
                    return 0;

                return index.Count;
            }
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                            CONSTRUCTEURS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public FolderListDepthIterator(FolderList folders)
        {
            source = folders;
            Reset();
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                           METHODES PUBLIC                         *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public bool MoveNext()
        {
            bool firstIteration = !rootReaded;
            if (!rootReaded)
            {
                rootReaded = true;
            }

            if (Current is FolderList) // On descend dans le composite
            {
                memory.Push(Current as FolderList);
                index.Push(0); // L'ordre est important, car Current est déterminé par l'index, qui est modifié ici !
            }
            else if (!firstIteration)
            {
                // On reste au même niveau, mais on passe à l'élément suivant
                int i = index.Pop() + 1;
                index.Push(i);
            }


            // On remonte dans l'historique si le dernier élément a été atteind
            while (index.Peek() >= memory.Peek().Files.Count)
            {
                index.Pop();

                if (index.Count == 0) return false; // Fin du parcours, nous avons retirer l'élément racine

                int i = index.Pop() + 1;
                index.Push(i);

                memory.Pop();
            }

            return memory.Count > 0;
        }

        public void Reset()
        {
            memory.Clear();
            index.Clear();

            memory.Push(source);
            index.Push(0);
        }
    }
}

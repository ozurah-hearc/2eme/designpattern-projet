﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

using ProjetDP.Iterator;
using System.Collections.Generic;

namespace ProjetDP.Composite
{
    /// <summary>
    /// Classe servant à représenter un dossier utilisant une liste pour ses enfants. 
    /// </summary>
    public class FolderList : File, IFolder
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                               CHAMPS                              *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public List<File> Files { get; private set; } = new List<File>();

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                            CONSTRUCTEURS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        ///  Constructeur de la classe FolderList
        /// </summary>
        /// <param name="name">Nom du dossier</param>
        public FolderList(string name)
        {
            Name = name;
            Size = 0;
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                           METHODES PUBLIC                         *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Ajouter un dossier ou un fichier au dossier
        /// </summary>
        /// <param name="file">Fichier / Dossier</param>
        public void Add(File file)
        {
            Files.Add(file);
        }

        /// <summary>
        /// Supprimer un dossier ou un fichier au dossier
        /// </summary>
        /// <param name="file">Fichier / Dossier</param>
        public void Remove(File file)
        {
            Files.Remove(file);
        }

        /// <summary>
        /// Retourne la taille total du dossier et de son contenu
        /// </summary>
        /// <returns>Taille total</returns>
        public override long GetTotalSize()
        {
            long result = Size;
            IIterator<File> enumerator = GetIterator();
            while (enumerator.MoveNext())
                result += enumerator.Current.Size;
            return result;
        }

        public IIterator<File> GetIterator()
        {
            return new FolderListDepthIterator(this);
        }
    }
}

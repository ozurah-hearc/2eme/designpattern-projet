﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

namespace ProjetDP.Composite
{
    /// <summary>
    /// Classe servant à repérsenter un document
    /// </summary>
    public class Document : File
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                            CONSTRUCTEURS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Constructeur de la classe Document
        /// </summary>
        /// <param name="name">Nom du document</param>
        /// <param name="size">Taille en octer du fichier</param>
        public Document(string name, long size)
        {
            Name = name;
            Size = size;
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                           METHODES PUBLIC                         *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Retourne la taille du document
        /// </summary>
        /// <returns>Taille du document</returns>
        public override long GetTotalSize()
        {
            return Size;
        }
    }
}

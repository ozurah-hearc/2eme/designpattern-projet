﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

namespace ProjetDP.Composite
{
    /// <summary>
    /// Classe représentant un fichier
    /// </summary>
    public abstract class File
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                             PROPRIETES                            *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Nom du fichier
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Taille du fichier
        /// </summary>
        public long Size { get; protected set; }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                         METHODES ABSTRAITES                       *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Retourne la taille total de lui même et de son contenu
        /// </summary>
        /// <returns>Taille total</returns>
        public abstract long GetTotalSize();
    }
}

﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/


using ProjetDP.Iterator;

namespace ProjetDP.Composite
{
    /// <summary>
    /// Interface permettant d'implémenter un dossier
    /// </summary>
    public interface IFolder : IIterable<File>
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                           METHODES PUBLIC                         *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Ajouter un dossier ou un fichier au dossier
        /// </summary>
        /// <param name="file">Fichier / Dossier</param>
        void Add(File file);

        /// <summary>
        /// Supprimer un dossier ou un fichier du dossier
        /// </summary>
        /// <param name="file">Fichier / Dossier</param>
        void Remove(File file);
    }
}

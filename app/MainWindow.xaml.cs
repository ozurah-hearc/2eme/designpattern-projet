﻿/*----------------------------------
  Design Patterns
  Projet : Composite, Iterator, Command sur une arborescence de dossiers
  Equipe 04 :	M. Allemann Jonas
                M. Jeannin Vincent
                M. Stouder Xavier
----------------------------------*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using ProjetDP.Command;
using ProjetDP.Composite;
using ProjetDP.Iterator;

namespace ProjetDP
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                               CHAMPS                              *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        private const string EXIST = "Ce nom de dossier / fichier existe.";
        private const string NOT_EXIST = "Ce nom de dossier / fichier n'existe pas.";
        private const string NOT_OPENED = "Aucun dossier ouvert";
        
        private readonly List<Tuple<string, ICommand>> searchCommandHistory = new List<Tuple<string, ICommand>>();

        private FolderList root; // Dossier racine actuellement ouvert

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                            CONSTRUCTEURS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        public MainWindow()
        {
            InitializeComponent();

            root = null;
            SetSearchHistoryState(false);

            UpdateTreeView();
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                          PRIVATE METHODS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Recherche le nom du fichier / dossier dans l'arborescence
        /// </summary>
        /// <param name="name">Recherche</param>
        public string Search(string name)
        {
            IIterator<Composite.File> enumerator = root.GetIterator();

            do // En faisant un do-while, on prend la racine de l'iterator)
            {
                Composite.File currentFile = enumerator.Current;
                if (currentFile.Name == name)
                {

                    lblSize.Content = enumerator.Current.GetTotalSize();
                    lblResultSearch.Content = EXIST;
                    return currentFile.Name;
                }
            } while (enumerator.MoveNext());

            lblSize.Content = "-";
            lblResultSearch.Content = NOT_EXIST;
            return null;
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
		|*                          PRIVATE METHODS                          *|
		\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Formatte le nom afficher pour un fichier
        /// </summary>
        /// <param name="file">Fichier</param>
        /// <returns>Nom formatter</returns>
        private string FileToString(Composite.File file)
        {
            return $"{file.Name}  ({file.GetType().Name})";
        }

        /// <summary>
        /// Permet d'ajouter un élément dans un TreeViewItem
        /// </summary>
        /// <param name="tvi">TreeViewItem</param>
        /// <param name="file">Fichier à ajouter</param>
        private void TvAddItem(TreeViewItem tvi, Composite.File file)
        {
            tvi.Items.Add(FileToString(file));
        }

        /// <summary>
        /// Permet d'ajouter un titre à un TreeViewItem
        /// </summary>
        /// <param name="tvi">TreeViewItem</param>
        /// <param name="file">Fichier</param>
        private void TvSetHeader(TreeViewItem tvi, Composite.File file)
        {
            tvi.Header = FileToString(file);
        }

        /// <summary>
        /// Met à jour l'historique de recherche.
        /// </summary>
        private void UpdateHistoryGrid()
        {
            historyGrid.Children.Clear();

            int i = 0;
            foreach (Tuple<string, ICommand> tuple in searchCommandHistory)
            {
                Label label = new Label
                {
                    Content = tuple.Item1
                };
                Button btn = new Button
                {
                    Content = "Rechercher",
                };
                btn.Click += (sender, args) => tuple.Item2.Execute();
                Grid.SetColumn(label, 0);
                Grid.SetRow(label, i);
                Grid.SetColumn(btn, 1);
                Grid.SetRow(btn, i);
                i++;

                historyGrid.Children.Add(label);
                historyGrid.Children.Add(btn);
            }
        }

        /// <summary>
        /// Met à jour le tree view en se basant sur l'élément racine <see cref="root"/>
        /// </summary>
        /// <remarks>Parcours en utilise l'iterateur créer pour le projet</remarks>
        private void UpdateTreeView()
        {
            trvFiles.Items.Clear();

            if (root == null)
            {
                TreeViewItem noValue = new TreeViewItem();
                noValue.Header = NOT_OPENED;
                trvFiles.Items.Add(noValue);
                return;
            }

            TreeViewItem treeViewItemRoot = new TreeViewItem();
            TvSetHeader(treeViewItemRoot, root);

            IIterator<Composite.File> enumerator = root.GetIterator();

            Stack<TreeViewItem> memory = new Stack<TreeViewItem>();
            memory.Push(treeViewItemRoot);

            int previousDepth = 0;

            while (enumerator.MoveNext()) // En faisant un while{}, nous ne prennons pas le 1er item qui est la racine
            {
                TreeViewItem tvi = new TreeViewItem();

                Composite.File item = enumerator.Current;
                int depth = enumerator.Depth;

                Console.WriteLine(depth + " --> " + item.Name);

                Console.WriteLine(item.Name);
                TvSetHeader(tvi, item);

                int deltaDepth = previousDepth - depth;
                while (deltaDepth >= 0)
                {
                    memory.Pop();
                    deltaDepth--;
                }

                memory.Peek().Items.Add(tvi);

                //if (previousDepth <= depth)
                memory.Push(tvi);
                previousDepth = depth;
            }


            trvFiles.Items.Add(treeViewItemRoot);

            // Affiche toute l'arborescence
            treeViewItemRoot.ExpandSubtree();
        }

        /// <summary>
        /// Peuple le composite en fonction des fichiers et sous dossier présent dans la racine spécifiée
        /// </summary>
        /// <param name="rootPath">Racine des dossiers à obtenir</param>
        /// <returns>Arborescence des dossiers/fichiers prêt à l'emploi</returns>
        private FolderList FillRootComposite(string rootPath)
        {
            FolderList root = new FolderList("/");

            Stack<IFolder> memoryFolder = new Stack<IFolder>();
            memoryFolder.Push(root);
            int previousDepth = Path.GetFullPath(rootPath).Split(Path.DirectorySeparatorChar).Length;

            string[] openedFiles = Directory.GetFileSystemEntries(rootPath, "*", SearchOption.AllDirectories); // Result as "Breadth-first"
            Array.Sort(openedFiles); // Without sort, the folders will firstly get, and next files // Result as "Depth-first"
            foreach (string file in openedFiles)
            {

                FileAttributes attr = System.IO.File.GetAttributes(file);

                string name = Path.GetFileName(file);
                Console.WriteLine(file);
                int depth = Path.GetFullPath(file).Split(Path.DirectorySeparatorChar).Length;

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    Console.WriteLine("DIR=" + name);
                    FolderList folder = new FolderList(name);

                    int deltaDepth = previousDepth - depth;
                    while (deltaDepth > 0)
                    {
                        memoryFolder.Pop();
                        deltaDepth--;
                    }

                    memoryFolder.Peek().Add(folder);

                    memoryFolder.Push(folder);
                }
                else
                {
                    int deltaDepth = previousDepth - depth;
                    while (deltaDepth > 0)
                    {
                        memoryFolder.Pop();
                        deltaDepth--;
                    }

                    Console.WriteLine("FILE=" + name);
                    long size = new FileInfo(file).Length;
                    Document doc = new Document(name, size);
                    memoryFolder.Peek().Add(doc);
                }

                previousDepth = depth;
            }

            return root;
        }

        /// <summary>
        /// Permet d'ouvrir un dossier.
        /// Le dossier ouvert va peupler le composite <see cref="root"/>.
        /// Les champs utilisateur "recherche" et "historique" seront activé / actualisé en conséquence 
        /// </summary>
        private void OpenFolder()
        {
            System.Windows.Forms.FolderBrowserDialog openFileDlg = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = openFileDlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string path = openFileDlg.SelectedPath;
                root = FillRootComposite(path);

                SetSearchHistoryState(true);
            }
            else
            {
                root = null;
                SetSearchHistoryState(false);
            }

            UpdateTreeView();

        }

        /// <summary>
        /// Met à jour l'état d'activation des composants "recherche" et "historique" + les informations contenue
        /// en fonction de l'état spécifié (false = désactivé/aucun contenu // true = activé/root est défini
        /// </summary>
        /// <param name="state"></param>
        private void SetSearchHistoryState(bool state)
        {
            searchGrid.IsEnabled = state;
            historyGrid.IsEnabled = state;

            if (state)
            {
                lblSize.Content = root.GetTotalSize().ToString();
                tbxSearch.Text = root.Name;
                Search(root.Name);
            }
            else
            {
                lblSize.Content = 0;
                tbxSearch.Text = "";
            }
        }

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
        |*                          EVENTS                                   *|
        \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

        /// <summary>
        /// Gère le clique de recherche.
        /// </summary>
        /// <param name="sender">émetteur</param>
        /// <param name="e">évenement</param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            // Création de la commande
            SearchCommand command = new SearchCommand(tbxSearch.Text, this);

            // Stockage de la commande dans l'historique TODO : update column

            searchCommandHistory.Insert(0, new Tuple<string, ICommand>(tbxSearch.Text, command));
            if (searchCommandHistory.Count > 10)
            {
                searchCommandHistory.RemoveRange(10, searchCommandHistory.Count - 10);
            }

            UpdateHistoryGrid();

            // Exécution de la commande
            command.Execute();
        }

        /// <summary>
        /// Execute la recherche quand la touche "enter" est pressée.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbxSearch_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                BtnSearch_Click(sender, e);
            }
        }

        /// <summary>
        /// Execute l'ouverture d'un dossier + les comportements associés
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            OpenFolder();
        }
    }
}
